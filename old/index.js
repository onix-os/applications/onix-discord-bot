const { Client, Intents } = require('discord.js');
const client = new Client({ intents: [Intents.FLAGS.GUILDS] });
const { clientId, guildId, token } = require('./config.json');

let readlineSync = require('readline-sync');
let Parser = require('rss-parser');
let parser = new Parser();


let builds = [];
let sended = [];

let checkRss = function(){
  (async () => {

    let feed = await parser.parseURL('https://sourceforge.net/projects/onixos/rss?path=/Daily');
  
    feed.items.forEach(item => {
      let build = {
        title: item.title,
        link: item.link,
        description: item.description,
        pubDate: item.pubDate
      };
      builds.push(build);
    });
  
  })();
};

let intervalUpdate = function(channel){
  setInterval(() => {
    checkRss();
    if(builds.length > 0){
      if(sended.indexOf(builds[0].link) == -1){
        sended.push(builds[0].link);
        channel.send("Latest build: "+builds[0].link);
      }
    }
  }, 60000);
};


client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}`)

    client.channels.fetch("879466239947321375").then(channel => {
      console.log(channel)
      intervalUpdate(channel);
    });

    
    console.log(client)
});



client.on('interactionCreate', async interaction => {
  console.log(interaction)
	if (!interaction.isCommand()) return;

	const { commandName } = interaction;

	if (commandName === 'ping') {
		await interaction.reply('Pong!');
	} else if (commandName === 'server') {
		await interaction.reply(`Server name: ${interaction.guild.name}\nTotal members: ${interaction.guild.memberCount}`);
	} else if (commandName === 'user') {
		await interaction.reply(`Your tag: ${interaction.user.tag}\nYour id: ${interaction.user.id}`);
	}
});


client.login(token);


