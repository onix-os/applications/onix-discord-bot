# Discord - Node.js

A Discord bot written in Node.js including a Postgres Database and customisable features.

# Installation

The bot runs in a Docker container that includes all that it needs to operate.  
You will need to add the relevant ID's and your Token to a config file.

## Requirements

- Docker Engine
- Discord API Bot Token

## Instructions

- `git clone` or download the repository.
- Open the file at `node/src/discord.json` and edit as seen [here](#discordjson-configuration-file)

# Starting the Bot

In order to start the bot, the individual components need to be built.

## First-time Setup

- Ensure that the Docker Engine is running. On Windows and Mac, launch Docker Desktop.
- Open a terminal in the `discord-nodejs` directory.
- Run `docker compose build` to build the images.

## Start the Bot

- If the Docker Engine isn't still up, start it.
- In the `discord-nodejs` directory, run the command `docker compose up` to start it up.

# discord.json Configuration File
This file houses all the information unique to your bot, like it's token and server info.

```js
{
	"token": "BOT_TOKEN", // Paste your Discord API Bot Token here
	"guild": {
		"id": "SERVER_ID", // Paste the Guild ID of the server HERE
		"welcome": "WELCOME_CHANNEL_ID", // Paste the Channel ID of the welcome message channel HERE
		"roles": [
			// This is the only required role on the list, it will be assigned to every member that joins
			{
				"name": "DEFAULT_ROLE_NAME",
				"id": "DEFAULT_ROLE_ID", // Paste the Role ID of your member role HERE
				"messages": 0 // Do not change this
			},

			// You can add more roles HERE by copying the same layout as the role above
		]
	}
}
```

# Utilisation

Now you're ready to start using the bot on your server!  
While the bot is running, you can access your commands by typing a `/` in the chat of the server you set in `discord.json`

# Attribution

- Written by - [James Barret](https://github.com/jamezburritos)

- Discord API Wrapper - [discord.js](https://discord.js.org/)
- PostgreSQL Database - [postgres](https://www.postgresql.org/)
