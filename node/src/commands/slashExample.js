module.exports = {
	type: 1,
	name: "slash",
	description: "an example slash command",
	async execute(interaction) {
		await interaction.reply({
			content: "hello. this is an example slash command",
			ephemeral: true
		});
	}
};