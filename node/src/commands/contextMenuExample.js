module.exports = {
	type: 3,
	name: "push me!",
	async execute(interaction) {
		const targetMessage = await interaction.channel.messages.fetch(interaction.targetId);

		interaction.reply({
			content: `you pushed the button on a message that said: \`${targetMessage.content}\``,
			ephemeral: true
		});
	}
};