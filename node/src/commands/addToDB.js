const { query } = require("../functions/query");

module.exports = {
	type: 2,
	name: "add to database",
	async execute(interaction) {
		const targetMember = interaction.guild.members.cache.get(interaction.targetId);

		await query({
			query: `INSERT INTO members (id, username, message_count, role)
					SELECT $1, $2, 0, $3
					WHERE NOT EXISTS (SELECT id FROM members WHERE id = $1)`,
			values: [targetMember.id, targetMember.user.username, targetMember.roles.highest.name]
		});
	}
};