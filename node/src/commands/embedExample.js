const { MessageEmbed } = require("discord.js");

module.exports = {
	type: 1,
	name: "embed",
	description: "an example command that responds with an embed",
	async execute(interaction) {
		const embed = await new MessageEmbed({
			title: "this is an embed",
			description: "you've sent an embed. embeds can contain significantly more information than a regular message",
			color: "#FFCC4F",
			url: "https://www.youtube.com/watch?v=dQw4w9WgXcQ",
			fields: [
				{
					name: "this is a field",
					value: "fields can organise information in embeds to make them more readable",
					inline: false
				}
			],
			thumbnail: {
				url: "https://i.imgur.com/ZXomd0L.png",
				width: 128,
				height: 128
			},
			author: {
				name: "jamezburritos",
				url: "https://github.com/jamezburritos",
				iconURL: "https://avatars.githubusercontent.com/u/69323892?v=4"
			},
			footer: {
				text: "this embed was sent by a discord-nodejs bot"
			}
		});

		await interaction.reply({
			embeds: [embed],
			ephemeral: true
		});
	}
};