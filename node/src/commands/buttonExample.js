const { MessageActionRow } = require("discord.js");

module.exports = {
	type: 1,
	name: "button",
	description: "an example command with a button component",
	async execute(interaction) {
		const actionRow = new MessageActionRow({
			components: [
				{
					type: "BUTTON",
					label: "push me!",
					customId: "exampleButton",
					style: "PRIMARY"
				}
			]
		});

		await interaction.reply({
			content: "click the test button below.",
			components: [actionRow],
			ephemeral: true
		});
	}
};