module.exports = {
	type: 1,
	name: "options",
	description: "an example command with options",
	options: [
		{
			type: "STRING",
			name: "echo",
			description: "this option will be echoed back to you",
			required: true
		},
		{
			type: "BOOLEAN",
			name: "ephemeral",
			description: "this option decides whether or not the response will stay in chat",
			required: false
		}
	],
	async execute(interaction) {
		const optionEphemeral = interaction.options.getBoolean("ephemeral", false);

		interaction.reply({
			content: `you used a command with options: \`${interaction.options.getString("echo", true)}\``,
			ephemeral: (optionEphemeral !== null) ? optionEphemeral : true
		});
	}
};