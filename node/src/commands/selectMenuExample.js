const { MessageActionRow } = require("discord.js");

module.exports = {
	type: 1,
	name: "selection",
	description: "an example command with a selection menu component",
	async execute(interaction) {
		const actionRow = new MessageActionRow({
			components: [
				{
					type: "SELECT_MENU",
					customId: "exampleMenu",
					placeholder: "select an option...",
					options: [
						{
							label: "hello world",
							value: "helloWorld",
							description: "sends \"hello world\" in chat"
						},
						{
							label: "uwu",
							value: "uwu",
							description: "sends \"uwu\" in chat"
						}
					]
				}
			]
		});

		await interaction.reply({
			content: "choose an option below: ",
			components: [actionRow],
			ephemeral: true
		});
	}
};