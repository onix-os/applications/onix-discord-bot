const { query } = require("../functions/query.js");
const { guild } = require("../discord.json");

module.exports = {
	name: "messageCreate",
	once: false,
	async execute(message, client) {
		if (message.author === client.user) return;

		await query({
			query: `UPDATE members
					SET message_count = message_count + 1
					WHERE id = $1`,
			values: [message.author.id]
		});

		const result = await query({
			query: `SELECT message_count FROM members
					WHERE id = $1`,
			values: [message.author.id]
		});

		const count = result.rows[0].message_count;

		Object.entries(guild.roles).forEach(([key, role]) => {
			if (count === role.messages) {
				message.member.roles.add(role.id);

				query({
					query: `UPDATE members
							SET role = $1
							WHERE id = $2`,
					values: [key, message.author.id]
				});
			}
		});
	}
};