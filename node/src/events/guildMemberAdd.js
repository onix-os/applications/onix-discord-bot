const { guild } = require("../discord.json");
const { query } = require("../functions/query.js");

module.exports = {
	name: "guildMemberAdd",
	once: false,
	async execute(member, client) {
		const welcomeChannel = client.channels.cache.get(guild.welcome);
		welcomeChannel.send(`welcome to the server - <@${member.id}>`);

		await query({
			query: `INSERT INTO members (id, username, message_count, role)
					SELECT $1, $2, 0, $3
					WHERE NOT EXISTS (SELECT id FROM members WHERE id = $1)`,
			values: [member.id, member.user.username, guild.roles[0]]
		});

		await member.roles.add(guild.roles[0].id);
	}
};