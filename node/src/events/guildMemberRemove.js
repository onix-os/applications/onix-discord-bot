const { query } = require("../functions/query");

module.exports = {
	name: "guildMemberRemove",
	once: "false",
	execute(member) {
		query({ 
			query: `DELETE FROM members
			WHERE id = $1`,
			values: [member.id]
		});
	}
};