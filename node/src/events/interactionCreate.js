const { readdirSync } = require("fs");

const commands = readdirSync("src/commands").filter(file => file.endsWith(".js"));
const components = readdirSync("src/components").filter(file => file.endsWith(".js"));

module.exports = {
	name: "interactionCreate",
	once: false,
	async execute(interaction) {
		if (interaction.isCommand() || interaction.isContextMenu()) {
			commands.forEach(file => {
				const { name, execute } = require(`../commands/${file}`);
				if (interaction.commandName === name) execute(interaction);
			});

			const data = interaction.isCommand() && interaction.options.data;

			let target = (interaction.isContextMenu() && interaction.targetType === "USER") && interaction.guild.members.cache.get(interaction.targetId).user.tag;
				target = (interaction.isContextMenu() && interaction.targetType === "MESSAGE") && interaction.channel.messages.cache.get(interaction.targetId).content;

			console.log(`[interaction] ${interaction.user.tag} used a ${(interaction.isCommand()) ? "command" : "context menu"} - ${interaction.commandName} `);

			if (interaction.isCommand() && data.length > 0) console.log(`[args]${data.map(opt => " " + opt.name + ": " + opt.value)}`);		
			else if (interaction.isContextMenu()) console.log(`[target] ${interaction.targetType.toLowerCase()} - ${target}`);
		}

		else if (interaction.isMessageComponent()) {
			components.forEach(file => {
				const { customId, execute } = require(`../components/${file}`);
				if (interaction.customId === customId) execute(interaction);
			});
		}
	}
};