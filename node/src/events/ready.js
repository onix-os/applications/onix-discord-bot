const { readdirSync } = require("fs");
const { generateLink } = require("../functions/generateLink.js");
const { guild } = require("../discord.json");

const files = readdirSync("src/commands").filter(file => file.endsWith(".js"));

module.exports = {
	name: "ready",
	once: true,
	async execute(client) {
		console.log(`[bot] client logged in - ${client.user.tag}`);

		const commands = files.map(file => {
			const { type, name, description, options, default_permission } = require(`../commands/${file}`);
			return { type, name, description, options, default_permission };
		});

		await client.application.commands.set(commands, guild.id);

		console.log(`[bot] client registered - ${commands.length} ${(commands.length > 1) ? "commands" : "command"}`);

		console.log("[bot] add the bot to your server - " + await generateLink(client, {
			scopes: [
				"applications.commands",
				"bot"
			],
			permissions: [
				"ADMINISTRATOR"
			]
		}));

		console.log("");
	}
};