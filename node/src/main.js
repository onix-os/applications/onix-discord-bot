const {	Client, Intents } = require("discord.js");
const { readdirSync } = require("fs");
const { token } = require("./discord.json");

const client = new Client({
	intents: [
		Intents.FLAGS.DIRECT_MESSAGES,
		Intents.FLAGS.GUILDS,
		Intents.FLAGS.GUILD_MESSAGES,
		Intents.FLAGS.GUILD_INVITES,
		Intents.FLAGS.GUILD_MEMBERS
	] 
});

const events = readdirSync("src/events").filter(file => file.endsWith(".js"));

events.forEach(event => {
	const { name, once, execute } = require(`./events/${event}`);

	if (once) {
		client.once(name, (...args) => execute(...args, client));
	} else {
		client.on(name, (...args) => execute(...args, client));
	}
});

client.login(token);