const lib = require("lib");

module.exports.generateLink = async (client, data) => {
	const link = client.generateInvite(data);

	let result = await lib.url.temporary["@0.3.0"].create({
		url: link,
		ttl: 300
	});

	return result.link_url;
};