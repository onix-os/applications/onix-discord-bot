const { Pool } = require("pg");

const credentials = {
	user: "postgres",
	host: "postgres",
	database: "discord",
	password: "passwerd",
	port: 5432
};

module.exports.query = async data => {
	const pool = new Pool(credentials);
	const query = await pool.query(data.query, data.values);
	await pool.end();
	return query;
};

