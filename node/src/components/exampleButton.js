module.exports = {
	customId: "exampleButton",
	async execute(interaction) {
		await interaction.reply({
			content: "you pushed the button",
			ephemeral: true
		});
	}
};