module.exports = {
	customId: "exampleMenu",
	async execute(interaction) {
		if (interaction.values.includes("helloWorld")) {
			await interaction.reply({
				content: "hello world",
				ephemeral: true
			});
		}

		if (interaction.values.includes("uwu")) {
			await interaction.reply({
				content: "uwu",
				ephemeral: true
			});
		}
	}
};